Rails.application.routes.draw do
  root to: 'home#index'

  resources :orders, only: [:create]
end

class Address < ApplicationRecord
  validates :street, :number, :zipcode, :city, :state, presence: true
  validates :neighborhood, :country, :latitude, :longitude, presence: true
  validates :receiver_phone, :complement, presence: true

  belongs_to :shipping, optional: true
end

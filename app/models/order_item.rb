class OrderItem < ApplicationRecord
  belongs_to :order, optional: true

  has_one :item

  validates_numericality_of :unit_price, { greater_than_or_equal_to: 0 }
  validates_numericality_of :full_unit_price, { greater_than_or_equal_to: 0 }
  validates_numericality_of :quantity, { greater_than_or_equal_to: 0 }
end

class Payment < ApplicationRecord
  enum status: { active: 0, paid: 1, cancelled: 2, approved: 3 }
  enum payment_type: { debit_card: 0, credit_card: 1 }

  validates_numericality_of :transaction_amount, { greater_than_or_equal_to: 0 }
  validates_numericality_of :shipping_cost, { greater_than_or_equal_to: 0 }
  validates_numericality_of :total_paid_amount, { greater_than_or_equal_to: 0 }
  validates_numericality_of :installment_amount, { greater_than_or_equal_to: 0 }
  validates_numericality_of :taxes_amount, { greater_than_or_equal_to: 0 }

  belongs_to :order, optional: true
end

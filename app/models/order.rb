class Order < ApplicationRecord
  belongs_to :store

  has_many :payments
  has_many :order_items

  has_one :shipping

  enum status: { active: 0, paid: 1, cancelled: 2, approved: 3 }

  validates_numericality_of :total_amount, { greater_than_or_equal_to: 0 }
  validates_numericality_of :total_shipping, { greater_than_or_equal_to: 0 }
  validates_numericality_of :total_amount_with_shipping, { greater_than_or_equal_to: 0 }
  validates_numericality_of :paid_amount, { greater_than_or_equal_to: 0 }
end

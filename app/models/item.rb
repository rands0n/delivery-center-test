class Item < ApplicationRecord
  belongs_to :order_item, optional: true

  validates :title, presence: true
end

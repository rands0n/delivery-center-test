class Buyer < ApplicationRecord
  validates :first_name, :last_name, :email, presence: true
  validates :telephone, :nickname, presence: true
  validates :document_type, :document_number, presence: true
end

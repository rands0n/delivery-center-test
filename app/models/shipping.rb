class Shipping < ApplicationRecord
  has_one :address

  belongs_to :order, optional: true
end

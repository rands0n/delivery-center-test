class HomeController < ApplicationController
  def index
    render json: {
      version: 1,
      message: 'Delivery Center Test - Randson'
     }, status: :ok
  end
end

class OrdersController < ApplicationController
  def create
    @store = create_store
    @buyer = create_buyer
    @shipping = create_shipping
    @address = create_address
    @order_items = create_order_items
    @payments = create_payments
    @order = create_order

    response.headers['X-Sent'] = Date.current.strftime('%Ih%M %d/%m/%y')

    render json: {
      store_id: @store.id,
      closed_at: @order.closed_at,
      total_amount: @order.total_amount,
      total_shipping: @order.total_shipping,
      total_amount_with_shipping: @order.total_amount_with_shipping,
      paid_amount: @order.paid_amount,
      expired_at: @order.expired_at,
      items: @order_items,
      payments: @payments,
      shipping: @shipping
    }, status: :created
  end

  private

  def order_params
    params.permit(
      :externalCode, :storeId, :subTotal,
      :deliveryFee, :total, :country, :state,
      :city, :district, :street, :complement,
      :latitude, :longitude,
      :postalCode, :number, :dtOrderCreate,
      customer: [:externalCode, :name, :email, :contact],
      items: [:externalCode, :name, :quantity, :price, :total],
      payments: [:type, :value]
     )
  end

  def create_store
    Store.where(name: Faker::Games::Zelda.location).first_or_create
  end

  def create_address
    Address.create(
      street: order_params['street'],
      number: order_params['number'].to_i,
      comment: Faker::Coffee.notes,
      zipcode: order_params['postalCode'].to_i,
      city: order_params['city'],
      state: order_params['state'],
      complement: order_params['complement'],
      neighborhood: order_params['district'],
      country: order_params['country'],
      latitude: order_params['latitude'].to_f,
      longitude: order_params['longitude'].to_f,
      receiver_phone: @buyer,
      shipping: @shipping
    )
  end

  def create_buyer
    Buyer
      .where(email: order_params['customer']['email'])
      .first_or_create do |buyer|
      buyer.first_name = order_params['customer']['name'].split(' ')[0].capitalize
      buyer.last_name = order_params['customer']['name'].split(' ')[1].capitalize
      buyer.nickname = order_params['customer']['name'].sub(' ', '_').downcase
      buyer.telephone = order_params['customer']['contact']
      buyer.email = order_params['customer']['email']
      buyer.document_type = 'CPF'
      buyer.document_number = '50701036443'
    end
  end

  def create_order_items
    order_items = []

    order_params['items'].each do |it|
      item = Item.where(title: it['name']).first_or_create do |item|
        item.title = it['name']
      end

      order_items << OrderItem.create(
        item: item,
        quantity: it['quantity'].to_i,
        unit_price: it['price'].to_f,
        full_unit_price: it['total'].to_f
      )
    end

    order_items
  end

  def create_payments
    payments = []

    order_params['payments'].each do |payment|
      payments << Payment.create(
        installments: 1,
        payment_type: :credit_card,
        status: :paid,
        approved_at: Date.current,
        transaction_amount: payment['value'].to_f,
        taxes_amount: 0,
        shipping_cost: 0,
        total_paid_amount: payment['value'].to_f,
        installment_amount: payment['value'].to_f
      )
    end

    payments
  end

  def create_shipping
    Shipping.create(
      shipment_type: 'shipping'
    )
  end

  def create_order
    shipping_cost = @payments.map(&:shipping_cost).sum
    amount_cost = @payments.map(&:transaction_amount).sum

    Order.create(
      store: @store,
      total_amount: amount_cost,
      total_shipping: shipping_cost,
      total_amount_with_shipping: (amount_cost.to_f + shipping_cost.to_f).to_f,
      paid_amount: @payments.map(&:total_paid_amount).sum,
      status: :paid,
      created_at: order_params['dtOrderCreate'].to_datetime,
      shipping: @shipping,
      order_items: @order_items,
      payments: @payments
    )
  end
end

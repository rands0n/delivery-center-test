# Delivery Center Test - Randson

Test for Delivery Center - Randson

## Technologies

This project is using the following technologies:

- Docker
- Postgres
- Rails
- RSpec

## Requirements

First up, [you need docker and docker-compose](https://www.docker.com/get-started) to build the project.

## Installation

Once you have everything on [Requirements](#Requirements) section setup. More actions to do:

```
# Clone the project
git clone git@gitlab.com:rands0n/delivery-center-test.git
cd delivery-center-test

# copy and edit the configuration
cp .env.example .env

# build the project on your machine
docker-compose build

# execute the migrations on the project
docker-compose run --rm app bundle exec rails db:create db:migrate db:seed

# run the project with docker-compose
docker-compose up
```

And that's done. You'll be able to access on `http://localhost:3000` :D

## Tests

The test runner of this project is RSpec, to execute the tests run:

```
docker-compose run app --rm bundle exec rspec
```

## Database

We use Postgres to manage our data, we can access the database running the `rails console` or
using the `pgweb` container which is located on `http://localhost:8081` after the project is up.

To execute the migrations or drop/setup the database, run:

```
docker-compose run --rm app bundle exec rails db:drop db:create db:migrate db:seed
```

Also, if you want to access the rails console inside the container, run:

```
docker-compose run --rm app bundle exec rails console
```

Always remember that you have the `pgweb` too, you can access on `http://localhost:8081` :D

# License

MIT © Randson

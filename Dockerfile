FROM ruby:2.7.2

RUN apt-get update \
  && apt-get -y install --no-install-recommends \
  libpq-dev postgresql-client \
  libxml2-dev libxslt1-dev nodejs \
  && rm -rf /var/lib/apt/lists/*

ENV APP_HOME /app
ENV RAILS_ROOT $APP_HOME
ENV BUNDLE_GEMFILE=$APP_HOME/Gemfile BUNDLE_JOBS=2 BUNDLE_PATH=/bundle

RUN mkdir $APP_HOME

WORKDIR $APP_HOME

COPY Gemfile $APP_HOME/Gemfile
COPY Gemfile.lock $APP_HOME/Gemfile.lock

RUN gem install bundler
RUN bundle install

COPY . $APP_HOME

RUN rm -f /app/tmp/pids/server.pid

EXPOSE 3000

CMD ["rails", "server", "-b", "0.0.0.0"]

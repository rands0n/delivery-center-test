require 'rails_helper'

RSpec.describe "Orders Controller", type: :request do
  let(:store) { create(:store) }
  let(:shipping) { create(:shipping) }
  let(:address) { create(:address) }
  let(:params) { JSON.parse(File.open("#{Dir.pwd}/spec/fixtures/orders/create.json").read) }

  describe 'POST /orders' do
    it 'renders http status created' do
      post '/orders', params: params

      expect(response).to have_http_status :created
    end

    context 'the payload must includes a buyer' do
      it 'creates a new buyer if not exists' do
        post '/orders', params: params

        buyer = Buyer.find_by(email: params['customer']['email'])

        expect(buyer.email).to eq 'john@doe.com'
      end
    end

    context 'the payload must includes a address to be sent' do
      it 'creates a new address if not exists' do
        post '/orders', params: params

        address = Address.find_by(zipcode: params['postalCode'])

        expect(address.zipcode).to eq 85045020
      end
    end

    context 'the payload must includes a items to create' do
      it 'creates a items and order_items not associated yet with an order' do
        post '/orders', params: params

        item = Item.find_by(title: params['items'][0]['name'])

        expect(item.title).to eq 'Produto de Testes'
      end

      it 'creates order items not yet associated with an order' do
        post '/orders', params: params

        expect(OrderItem.count).to be >= 1
      end
    end

    context 'the payload must includes payments' do
      it 'creates a payment not yet associated with an order' do
        post '/orders', params: params

        payment = Payment.find_by(transaction_amount: params['payments'][0]['value'].to_f)

        expect(payment.payment_type).to eq 'credit_card'
      end

      it 'create payments not yet associated with an order' do
        post '/orders', params: params

        expect(Payment.count).to be >= 1
      end
    end

    context 'the result is an order with payments, buyer and order items' do
      it 'renders order items' do
        post '/orders', params: params

        expect(JSON.parse(response.body)['items'].count).to be >= 1
      end

      it 'renders payments' do
        post '/orders', params: params

        expect(JSON.parse(response.body)['payments'].count).to be >= 1
      end

      it 'renders total amount' do
        post '/orders', params: params

        expect(JSON.parse(response.body)['total_amount']).to eq 55.04
      end

      it 'renders total shipping' do
        post '/orders', params: params

        expect(JSON.parse(response.body)['total_shipping']).to eq 0.0
      end

      it 'renders total amount with shipping' do
        post '/orders', params: params

        expect(JSON.parse(response.body)['total_amount_with_shipping']).to eq 55.04
      end

      it 'the payload must includes a X-Sent header' do
        post '/orders', params: params

        expect(response.headers['X-Sent']).to be_present
      end
    end
  end
end

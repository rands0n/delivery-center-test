require 'rails_helper'

RSpec.describe "HomeController", type: :request do
  describe 'GET /' do
    it 'returns http status success' do
      get '/'

      expect(response).to have_http_status :ok
    end

    it 'includes the message about the test' do
      get '/'

      expect(JSON.parse(response.body)['message']).to eq 'Delivery Center Test - Randson'
    end
  end
end

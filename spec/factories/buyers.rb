FactoryBot.define do
  factory :buyer do
    nickname { "#{Faker::Name.first_name}_#{Faker::Name.last_name}".downcase! }
    email { Faker::Internet.email }
    telephone { Faker::PhoneNumber.cell_phone_in_e164 }
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
  end
end

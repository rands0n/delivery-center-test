FactoryBot.define do
  factory :item do
    order_item { order_item }
    title { Faker::Beer.name }
  end
end

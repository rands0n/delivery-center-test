FactoryBot.define do
  factory :address do
    street { Faker::Address.street_name }
    number { Faker::Address.building_number }
    comment { Faker::Coffee.notes }
    zipcode { Faker::Address.zip_code }
    complement { 'AP 2001' }
    city { Faker::Address.city }
    state { Faker::Address.state }
    neighborhood { "Bigorrilho" }
    country { Faker::Address.country }
    latitude { Faker::Address.latitude }
    longitude {Faker::Address.longitude }
    receiver_phone { Faker::PhoneNumber.cell_phone_in_e164 }

    shipping { shipping }
  end
end

FactoryBot.define do
  factory :shipping do
    shipment_type { 'shipping' }
  end
end

FactoryBot.define do
  factory :order do
    store { store }
    closed_at { nil }
    total_amount { 1.5 }
    total_shipping { 1.5 }
    total_amount_with_shipping { 1.5 }
    paid_amount { 1.5 }
    expired_at { nil }
    status { 1 }
  end
end

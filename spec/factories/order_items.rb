FactoryBot.define do
  factory :order_item do
    order { order }
    quantity { 2 }
    unit_price { 1.5 }
    full_unit_price { 3 }
  end
end

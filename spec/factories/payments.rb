FactoryBot.define do
  factory :payment do
    installments { rand(12) }
    payment_type { 1 } # credit_card
    status { 1 } # paid
    approved_at { Time.current - 2.days }
    transaction_amount { 1.5 }
    taxes_amount { 0 }
    shipping_cost { 2 }
    total_paid_amount { 3.5 }
    installment_amount { 1.5 }
  end
end

require 'rails_helper'

RSpec.describe Shipping, type: :model do
  let(:shipping) { create(:shipping) }

  describe 'Relationships' do
    it { is_expected.to have_one(:address) }
    it { is_expected.to belong_to(:order).optional }
  end
end

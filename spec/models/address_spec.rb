require 'rails_helper'

RSpec.describe Address, type: :model do
  let(:address) { create(:address) }

  describe 'Validations' do
    it { is_expected.to validate_presence_of(:street) }
    it { is_expected.to validate_presence_of(:number) }
    it { is_expected.to validate_presence_of(:zipcode) }
    it { is_expected.to validate_presence_of(:city) }
    it { is_expected.to validate_presence_of(:state) }
    it { is_expected.to validate_presence_of(:neighborhood) }
    it { is_expected.to validate_presence_of(:country) }
    it { is_expected.to validate_presence_of(:latitude) }
    it { is_expected.to validate_presence_of(:longitude) }
    it { is_expected.to validate_presence_of(:complement) }
    it { is_expected.to validate_presence_of(:receiver_phone) }
  end

  describe 'Relationships' do
    it { is_expected.to belong_to(:shipping).optional }
  end
end

require 'rails_helper'

RSpec.describe Payment, type: :model do
  let(:payment) { create(:payment) }

  describe 'Validations' do
    it { is_expected.to define_enum_for(:status) }
    it { is_expected.to define_enum_for(:payment_type) }

    it { is_expected.to validate_numericality_of(:transaction_amount).is_greater_than_or_equal_to(0) }
    it { is_expected.to validate_numericality_of(:taxes_amount).is_greater_than_or_equal_to(0) }
    it { is_expected.to validate_numericality_of(:shipping_cost).is_greater_than_or_equal_to(0) }
    it { is_expected.to validate_numericality_of(:total_paid_amount).is_greater_than_or_equal_to(0) }
    it { is_expected.to validate_numericality_of(:installment_amount).is_greater_than_or_equal_to(0) }
  end

  describe 'Relationships' do
    it { is_expected.to belong_to(:order).optional }
  end
end

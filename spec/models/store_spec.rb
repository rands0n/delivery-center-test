require 'rails_helper'

RSpec.describe Store, type: :model do
  let(:store) { create(:store) }

  describe 'Validations' do
    it { is_expected.to validate_presence_of(:name) }
  end

  describe 'Relationships' do
    it { is_expected.to have_many(:orders) }
  end
end

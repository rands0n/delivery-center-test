require 'rails_helper'

RSpec.describe Order, type: :model do
  let(:order) { create(:order) }

  describe 'Validations' do
    it { is_expected.to define_enum_for(:status) }

    it { is_expected.to validate_numericality_of(:paid_amount).is_greater_than_or_equal_to(0) }
    it { is_expected.to validate_numericality_of(:total_amount).is_greater_than_or_equal_to(0) }
    it { is_expected.to validate_numericality_of(:total_shipping).is_greater_than_or_equal_to(0) }
    it {
      is_expected.to validate_numericality_of(:total_amount_with_shipping).is_greater_than_or_equal_to(0)
    }
  end

  describe 'Relationships' do
    it { is_expected.to have_one(:shipping) }
    it { is_expected.to have_many(:payments) }
    it { is_expected.to have_many(:order_items) }
  end
end

require 'rails_helper'

RSpec.describe Item, type: :model do
  let(:item) { create(:item) }

  describe 'Validations' do
    it { is_expected.to validate_presence_of(:title) }
  end

  describe 'Relationships' do
    it { is_expected.to belong_to(:order_item).optional }
  end
end

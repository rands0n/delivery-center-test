require 'rails_helper'

RSpec.describe OrderItem, type: :model do
  let(:order_item) { create(:order_item) }

  describe 'Validations' do
    it { is_expected.to validate_numericality_of(:quantity).is_greater_than_or_equal_to(0) }
    it { is_expected.to validate_numericality_of(:unit_price).is_greater_than_or_equal_to(0) }
    it { is_expected.to validate_numericality_of(:full_unit_price).is_greater_than_or_equal_to(0) }
  end

  describe 'Relationships' do
    it { is_expected.to belong_to(:order).optional }
    it { is_expected.to have_one(:item) }
  end
end

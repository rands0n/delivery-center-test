class CreateOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :orders, id: :uuid do |t|
      t.references :store, null: false, foreign_key: true, type: :uuid
      t.timestamp :closed_at
      t.float :total_amount
      t.float :total_shipping
      t.float :total_amount_with_shipping
      t.float :paid_amount
      t.timestamp :expired_at
      t.integer :status

      t.timestamps
    end
  end
end

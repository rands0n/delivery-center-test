class CreateAddresses < ActiveRecord::Migration[6.0]
  def change
    create_table :addresses, id: :uuid do |t|
      t.string :street
      t.integer :number
      t.string :comment
      t.integer :zipcode
      t.string :city
      t.string :state
      t.string :neighborhood
      t.string :country
      t.float :latitude
      t.float :longitude
      t.string :receiver_phone

      t.timestamps
    end
  end
end

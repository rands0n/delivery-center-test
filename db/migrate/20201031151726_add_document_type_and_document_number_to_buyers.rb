class AddDocumentTypeAndDocumentNumberToBuyers < ActiveRecord::Migration[6.0]
  def change
    add_column :buyers, :document_type, :string
    add_column :buyers, :document_number, :string
  end
end

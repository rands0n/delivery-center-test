class AddShippingToAddress < ActiveRecord::Migration[6.0]
  def change
    add_reference :addresses, :shipping, type: :uuid, foreign_key: true
  end
end

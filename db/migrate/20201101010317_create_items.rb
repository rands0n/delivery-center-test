class CreateItems < ActiveRecord::Migration[6.0]
  def change
    create_table :items, id: :uuid do |t|
      t.references :order_item, foreign_key: true, type: :uuid
      t.string :title

      t.timestamps
    end
  end
end

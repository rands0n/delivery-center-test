class CreateShippings < ActiveRecord::Migration[6.0]
  def change
    create_table :shippings, id: :uuid do |t|
      t.string :shipment_type

      t.timestamps
    end
  end
end

class AddOrderToPayment < ActiveRecord::Migration[6.0]
  def change
    add_reference :payments, :order, foreign_key: true, type: :uuid
  end
end

class CreatePayments < ActiveRecord::Migration[6.0]
  def change
    create_table :payments, id: :uuid do |t|
      t.integer :installments
      t.integer :payment_type
      t.integer :status
      t.timestamp :approved_at
      t.float :transaction_amount
      t.float :taxes_amount
      t.float :shipping_cost
      t.float :total_paid_amount
      t.float :installment_amount

      t.timestamps
    end
  end
end

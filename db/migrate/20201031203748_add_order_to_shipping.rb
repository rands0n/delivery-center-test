class AddOrderToShipping < ActiveRecord::Migration[6.0]
  def change
    add_reference :shippings, :order, foreign_key: true, type: :uuid
  end
end

puts 'Creating stores...'
store = Store.create(name: Faker::Company.name)

puts 'Creating shippings...'
shipping = Shipping.create(
  shipment_type: "shipping"
)

puts 'Creating items...'
item = Item.create(
  title: Faker::Beer.name
)

puts 'Creating order items...'
order_item = OrderItem.create(
  item: item,
  unit_price: 5,
  full_unit_price: 10,
  quantity: 2
)

puts 'Creating payments...'
payment = Payment.create(
  installments: rand(12),
  payment_type: 1, # credit_card
  status: 1, # paid
  approved_at: Time.current - 2.days,
  transaction_amount: 1.5,
  taxes_amount: 0,
  shipping_cost: 2,
  total_paid_amount: 3.5,
  installment_amount: 1.5
)

puts 'Creating buyers...'
buyer = Buyer.create(
  nickname: "#{Faker::Name.first_name}_#{Faker::Name.last_name}".downcase!,
  email: Faker::Internet.email,
  telephone: Faker::PhoneNumber.cell_phone_in_e164,
  first_name: Faker::Name.first_name,
  last_name: Faker::Name.last_name,
  document_type: 'CPF',
  document_number: '43255472237'
)

puts 'Creating addresses...'
address = Address.create(
  street: Faker::Address.street_name,
  number: Faker::Address.building_number,
  comment: Faker::Coffee.notes,
  zipcode: Faker::Address.zip_code,
  city: Faker::Address.city,
  state: Faker::Address.state,
  complement: 'AP 404',
  neighborhood: "Bigorrilho",
  country: Faker::Address.country,
  latitude: Faker::Address.latitude,
  longitude:Faker::Address.longitude,
  receiver_phone: Faker::PhoneNumber.cell_phone_in_e164,
  shipping: Shipping.last
)

puts 'Creating orders...'
order = Order.create(
  store: store,
  order_items: [order_item],
  closed_at: nil,
  total_amount: 1.5,
  total_shipping: 1.5,
  total_amount_with_shipping: 1.5,
  paid_amount: 1.5,
  expired_at: nil,
  status: 1, # paid
  payments: [payment],
  shipping: shipping
)
